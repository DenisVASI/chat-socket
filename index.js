//connect libraries
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const Mutex = require('async-mutex').Mutex;

//create constants
const port = 5090;
const mutex = new Mutex();

//сюда сохраняем id пользователей, для примера, сделать что-то более надежное
let connectedUsers = {};

async function addToUsersList(userId, type) {
    const release = await mutex.acquire();
    try {
        let users_ar = connectedUsers[type] ? connectedUsers[type] : [];
        users_ar.push(userId);
        connectedUsers[type] = users_ar;
    } finally {
        release();
    }
}

async function deleteFromUserList(userId, type) {
    const release = await mutex.acquire();
    try {
        let users_ar = connectedUsers[type] ? connectedUsers[type] : [];
        users_ar = users_ar.filter(user => user !== userId);
        connectedUsers[type] = users_ar;
    } finally {
        release();
    }
}

async function isOnline(userId, type, callback) {
    if (connectedUsers[type] && (connectedUsers[type].indexOf(userId) + 1 && typeof callback === "function")) callback();
}

io.on('connection', async (socket) => {

    await addToUsersList(socket.id, socket.handshake.query.type);
    console.log(connectedUsers);
    socket.on('disconnect', async _ => {
        await deleteFromUserList(socket.id, socket.handshake.query.type);
    });

    socket.on('message-to-help', function (msg) {
        try {
            connectedUsers[msg.to].map(async user => {
                await isOnline(user, msg.to, _ => {
                    io.to(user).emit('message', {
                        from: msg.id,
                        message: msg.message
                    });
                })
            });
        } catch {
            socket.emit('error', {
                error: 'send messages failed'
            });
        }
    });

    socket.on('message-to-user', async function (msg) {
        try {
            await isOnline(msg.to, 'user', _ => {
                io.to(msg.to).emit('message', {
                    from: msg.id,
                    message: msg.message
                });
            })
        } catch {
            socket.emit('error', {
                error: 'send messages failed'
            });
        }
    });
});

//server
server.listen(port, () => {
    console.log(`server listen at port ${port}`);
});